#!/usr/bin/python3
""" Launcher for iapetal """

# pylint: disable=invalid-name
# pylint: disable=line-too-long

# Copyright (C) 2019 Gwyn Ciesla

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import getpass

import gi

gi.require_version("Gtk", "3.0")
# pylint: disable=wrong-import-position
from gi.repository import Gtk


class iapetal_launcher:
    """UI class"""

    # pylint: disable=too-many-instance-attributes

    def delete_event(self, widget, event, data=None):
        """Close"""

        # pylint: disable=unused-argument
        Gtk.main_quit()
        return False

    def aboutdial(self, widget, event, data=None):
        """About dialog"""

        # pylint: disable=unused-argument

        abouttext = "Rescue the people from the habitat module by ferrying them to the rescue ship flying overhead.\n\n"
        abouttext += "Land next to the habitat module to load people from it, and land on the desk of the rescue ship to unload them.\n"
        abouttext += "If you run out of landers, all the people in a level are killed, the game ends.\n"
        abouttext += "If the rescue ship is destroyed and there are still people, you can attempt to escape into space with them,\n"
        abouttext += "which will also end the level.\n"
        abouttext += "Your deflector shield offers limited protection from asteroids, the surface and atmospheric fricton.\n\n"
        abouttext += "Rotate the lander with the right and left arrows.  Up arrow is the main thruster.  Down is the weaker downward thruster.\n"
        abouttext += "Space is the shield. P pauses. S toggles sound. Q quits."

        abouts = Gtk.MessageDialog(
            parent=self.window,
            text=abouttext,
            destroy_with_parent=True,
            message_type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.CLOSE,
        )
        abouts.set_title("Iapetal © 2019 Gwyn Ciesla")
        abouts.run()
        abouts.destroy()

        return False

    def runthing(self, widget, data):
        """Do stuff"""

        # pylint: disable=unused-argument

        if os.path.lexists("/usr/bin/iapetal"):
            runobject = "/usr/bin/iapetal"
        else:
            runobject = "./iapetal.py"
        if self.windowedcheck.get_active():
            runobject = runobject + " -w"
        if not self.soundcheck.get_active():
            runobject = runobject + " -n"
        os.system(runobject)

        scorefilename = os.path.join(
            os.path.expanduser("~" + getpass.getuser()), ".iapetal_score"
        )
        if not os.path.isfile(scorefilename):
            open(  # pylint: disable=consider-using-with
                scorefilename, "a", encoding="utf-8"
            ).close()
        with open(scorefilename, "r", encoding="utf-8") as scorefile:
            oldscore = scorefile.readline()
        if not oldscore:
            oldscore = 0
        scoretext = "High Score: " + str(oldscore)
        self.scorelabel.set_text(scoretext)

    def __init__(self):
        """Init"""

        # pylint: disable=too-many-statements

        self.window = Gtk.Window(type=Gtk.WindowType.TOPLEVEL)
        self.window.set_title("Iapetal")
        self.window.set_border_width(10)
        self.window.connect("delete_event", self.delete_event, None)

        self.mainbox = Gtk.VBox(homogeneous=False, spacing=0)
        self.window.add(self.mainbox)

        scorefilename = os.path.join(
            os.path.expanduser("~" + getpass.getuser()), ".iapetal_score"
        )
        if not os.path.isfile(scorefilename):
            open(  # pylint: disable=consider-using-with
                scorefilename, "a", encoding="utf-8"
            ).close()
        with open(scorefilename, "r", encoding="utf-8") as scorefile:
            oldscore = scorefile.readline()
        if not oldscore:
            oldscore = 0
        scoretext = "High Score: " + str(oldscore)
        self.scorelabel = Gtk.Label(label=scoretext)
        self.mainbox.pack_start(self.scorelabel, True, True, 0)

        if os.path.lexists("/usr/share/iapetal"):
            file_path = "/usr/share/iapetal/"
        else:
            file_path = "./"

        self.title = Gtk.Image()
        self.title.set_from_file(file_path + "title.png")
        self.mainbox.pack_start(self.title, True, True, 0)

        self.logo = Gtk.Image()
        self.logo.set_from_file(file_path + "habitat.png")
        self.mainbox.pack_start(self.logo, True, True, 0)

        self.runbutton = Gtk.Button(label="Go!")
        self.runbutton.connect("clicked", self.runthing, None)
        self.runbutton.set_tooltip_text("Play")
        self.mainbox.pack_start(self.runbutton, True, True, 0)

        self.windowedcheck = Gtk.CheckButton(label="Windowed")
        self.windowedcheck.set_tooltip_text("Run in windowed mode")
        self.mainbox.pack_start(self.windowedcheck, True, True, 0)

        self.soundcheck = Gtk.CheckButton(label="Sound")
        self.soundcheck.set_tooltip_text("Enable/disable sound")
        self.soundcheck.set_active(True)
        self.mainbox.pack_start(self.soundcheck, True, True, 0)

        self.docbutton = Gtk.Button(label="About")
        self.docbutton.connect("clicked", self.aboutdial, None)
        self.docbutton.set_tooltip_text("What's all this about?")
        self.mainbox.pack_start(self.docbutton, True, True, 0)

        self.quitbutton = Gtk.Button(label="Quit")
        self.quitbutton.connect("clicked", self.delete_event, None)
        self.quitbutton.set_tooltip_text("Exit Iapetal")
        self.mainbox.pack_start(self.quitbutton, True, True, 0)

        self.quitbutton.show()
        self.title.show()
        self.logo.show()
        self.scorelabel.show()
        self.docbutton.show()
        self.windowedcheck.show()
        self.soundcheck.show()
        self.runbutton.show()
        self.mainbox.show()
        self.window.show()


def main():
    """Main function"""

    Gtk.main()


if __name__ == "__main__":
    HELLO = iapetal_launcher()
    main()
