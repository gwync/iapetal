#!/usr/bin/python3
""" A lunar lander / rescue game """

# pylint: disable=too-many-lines
# pylint: disable=no-member

# Copyright (C) 2019 Gwyn Ciesla

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import random
import os.path
import getpass
import time
import math
import argparse
import pygame

PARSER = argparse.ArgumentParser(description="Iapetal space rescue game")
PARSER.add_argument("-v", "--version", action="version", version="1.4")
PARSER.add_argument(
    "-w",
    "--windowed",
    action="store_false",
    dest="windowed",
    help="Play in windowed mode",
)
PARSER.add_argument(
    "-n", "--nosound", action="store_false", dest="nosound", help="Disable sound"
)

ARGS = PARSER.parse_args()

pygame.init()
if ARGS.nosound:
    pygame.mixer.init(22050, -16, 16, 1024)

pygame.display.set_caption("Iapetal")

CLOCK = pygame.time.Clock()

SIZE = WIDTH, HEIGHT = 1024, 768
BLACK = 0, 0, 0

SCREEN = pygame.display.set_mode(SIZE)
if ARGS.windowed:
    pygame.display.toggle_fullscreen()

pygame.mouse.set_visible(0)

FLOOR = HEIGHT - 25

LEVEL = 1

LANDERS = 3
SCORE = 0
ENDLEVEL = 0
ENDGAME = 0

PEOPLE_IN_HABITAT = 16
PEOPLE_IN_LANDER = 0
PEOPLE_IN_LANDER_LIMIT = 4
PEOPLE_IN_RESCUE = 0
LOAD_DELAY = 0
LOAD_DELAY_MAX = 8

ROCK_DELAY = 0
ROCK_DELAY_MAX = 120


def stars_init(stars, width, floor):
    """Toss some random STARS upon the firmament"""

    stars = []
    for _ in range(0, 25):
        stars.append(
            (random.randrange(25, width - 25), random.randrange(25, floor - 25))
        )
    return stars


STARS = []
STARS = stars_init(STARS, WIDTH, FLOOR)

FONT = pygame.font.SysFont("DejaVu Sans", 12)
BIG_FONT = pygame.font.SysFont("DejaVu Sans", 48)
COLOR = (0, 255, 255)
INFOBAR_H = HEIGHT - 20

DEAD_TEXT = BIG_FONT.render("Placeholder", 1, COLOR)
LOST_TEXT = BIG_FONT.render("Placeholder", 1, COLOR)

if os.path.lexists("/usr/share/iapetal"):
    FILE_PATH = "/usr/share/iapetal/"
else:
    FILE_PATH = "./"

LANDER = pygame.image.load(FILE_PATH + "lander.png")
LANDER_THRUST = pygame.image.load(FILE_PATH + "lander_thrust.png")
BOOM = pygame.image.load(FILE_PATH + "boom.png")
LANDERRECT = LANDER.get_rect()
LANDER_BURN = pygame.image.load(FILE_PATH + "burn_medium.png")
LANDER_BURNRECT = LANDER_BURN.get_rect()


class Rocks:
    """Rocks"""

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-few-public-methods

    hval = 0
    vval = 0
    speed_h = 0
    speed_v = 0
    DIR = 0
    spin = 0
    image = 0
    image_boom = 0
    rect = 0
    CENTERHOLD = 0, 0
    new = 0
    mass = 0
    burning = 0
    burn_image = 0
    burnrect = 0
    burnnew = 0


ROCKLIST = []

BASE = pygame.image.load(FILE_PATH + "base.png")
BASE_BOOM = pygame.image.load(FILE_PATH + "base_boom.png")
BASERECT = BASE.get_rect()
BASE_OUT = 0

HABITAT = pygame.image.load(FILE_PATH + "habitat.png")
HABITAT_BOOM = pygame.image.load(FILE_PATH + "habitat_boom.png")
HABITATRECT = HABITAT.get_rect()
HABITAT_OUT = 0
HABITAT_OUT_COUNT = 5


class RESCUES:
    """Rescue ship"""

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-few-public-methods

    hval = 0
    vval = 0
    speed_h = 0
    speed_v = 0
    orig_speed_h = 0
    image_left = 0
    image_left_boom = 0
    image_right = 0
    image_right_boom = 0
    CENTER = 0
    rect = 0
    new = 0
    orient_mult = 0
    out = 0
    entering = 0


def rescue_init(width):
    """Set up the rescue ship"""

    rescue = RESCUES()

    rescue.entering = 1
    orient = random.randrange(-1, 1)
    if orient >= 0:
        rescue.orient_mult = 1
    else:
        rescue.orient_mult = -1
    rescue.hval = random.randrange(50, width - 50)
    rescue.vval = random.randrange(-25, 0)
    rescue.speed_h = random.randrange(1, 2) * rescue.orient_mult
    rescue.speed_v = 0
    rescue.orig_speed_h = rescue.speed_h

    rescue.image_right = pygame.image.load(FILE_PATH + "rescue_right.png")
    rescue.image_right_boom = pygame.image.load(FILE_PATH + "rescue_boom_right.png")
    rescue.image_left = pygame.image.load(FILE_PATH + "rescue_left.png")
    rescue.image_left_boom = pygame.image.load(FILE_PATH + "rescue_boom_left.png")
    rescue.out = 0

    if rescue.orient_mult == 1:
        rescue.rect = rescue.image_right.get_rect()
    else:
        rescue.rect = rescue.image_left.get_rect()
    rescue.rect = rescue.rect.move([rescue.hval, rescue.vval])

    return rescue


THRUST_SOUND = pygame.mixer.Sound(FILE_PATH + "thrust.ogg")
CRASH_SOUND = pygame.mixer.Sound(FILE_PATH + "crash.ogg")
SHIELD_SOUND = pygame.mixer.Sound(FILE_PATH + "shield.ogg")
PICKUP_SOUND = pygame.mixer.Sound(FILE_PATH + "pickup.ogg")
DROPOFF_SOUND = pygame.mixer.Sound(FILE_PATH + "dropoff.ogg")
BURN_SOUND = pygame.mixer.Sound(FILE_PATH + "burn.ogg")
CHANNEL = pygame.mixer.find_channel()
THRUST_CHANNEL = pygame.mixer.find_channel()
SHIELD_CHANNEL = pygame.mixer.find_channel()
BURN_CHANNEL = pygame.mixer.find_channel()


def kabam(new, rect):
    """Does what it says on the tin"""
    SCREEN.blit(new, rect)
    pygame.display.flip()


def rock_load_images(thisrock, size):
    """Rocks are of several sizes and need different images"""
    filestring = ""
    if size == 4:
        filestring = ""
    if size == 3:
        filestring = "_medium"
    if size == 2:
        filestring = "_small"
    if size == 1:
        filestring = "_tiny"

    thisrock.image = pygame.image.load(FILE_PATH + "rock" + filestring + ".png")
    thisrock.image_boom = pygame.image.load(
        FILE_PATH + "rock" + filestring + "_boom.png"
    )
    thisrock.burn_image = pygame.image.load(FILE_PATH + "burn" + filestring + ".png")

    return thisrock


def rock_init(rocklist, width, height):
    """Set up a rock"""

    newrock = Rocks()

    newrock.hval = random.randrange(50, width - 50)
    newrock.vval = -random.randrange(0, height - 200)
    newrock.spin = random.randrange(-5, 5)
    newrock.speed_h = 0
    while newrock.speed_h == 0:
        newrock.speed_h = random.randrange(-15, 15)
    newrock.speed_v = random.randrange(0, 2)

    size = random.randrange(0, 100)

    if size >= 90:
        newrock = rock_load_images(newrock, 4)
        newrock.burnrect = newrock.burn_image.get_rect()
        newrock.mass = 4
    elif size >= 70:
        newrock = rock_load_images(newrock, 3)
        newrock.burnrect = newrock.burn_image.get_rect()
        newrock.mass = 3
    elif size >= 50:
        newrock = rock_load_images(newrock, 2)
        newrock.burnrect = newrock.burn_image.get_rect()
        newrock.mass = 2
    else:
        newrock = rock_load_images(newrock, 1)
        newrock.burnrect = newrock.burn_image.get_rect()
        newrock.mass = 1

    newrock.rect = newrock.image.get_rect()
    newrock.rect = newrock.rect.move([newrock.hval, newrock.vval])

    rocklist.append(newrock)

    return ROCKLIST


def wrap(xval, width):
    """In one side and out the other"""

    if xval < 0:
        xval = width - 12
    else:
        if xval > width:
            xval = 12
    return xval


def gravitize(speed, floor, yval):
    """Falling"""

    speed += (1 / math.pow((floor - yval) + 1000, 2)) * 100000
    return speed


def rockburst(rocklist, width, height):
    """Want five rocks? You get five rocks."""

    burst_count = 5
    while burst_count >= 0:
        rocklist = rock_init(rocklist, width, height)
        burst_count -= 1

    return rocklist


def level_init(baserect, base_out, base_out_count, habitatrect, width, floor):
    """Set up a fresh LEVEL"""

    # pylint: disable=too-many-arguments, too-many-positional-arguments

    base_h = random.randrange(50, width - 50)

    if base_out >= 0:
        base_out = 0
        base_out_count = 5
        base_v = floor - 14
        baserect = BASE.get_rect()
        baserect = baserect.move([base_h, base_v])

    habitat_h = base_h
    while abs(habitat_h - base_h) <= 60:
        habitat_h = random.randrange(50, width - 50)
    habitat_v = floor - 31
    habitatrect = HABITAT.get_rect()
    habitatrect = habitatrect.move([habitat_h, habitat_v])

    return baserect, base_out, base_out_count, habitatrect


def game_init(landerrect, rocklist, baserect, habitatrect, base_out, new_level, floor):
    """Start the game"""

    # pylint: disable=too-many-locals, too-many-arguments, too-many-positional-arguments

    direction = 0
    fuel = 200
    dead = 0
    lost = 0
    destroyed = 0
    deflector = 500

    lander_start_h = random.randrange(50, WIDTH - 50)
    lander_start_v = random.randrange(25, 300)
    lander_start_speed_h = random.randrange(-10, 10)
    lander_start_speed_v = random.randrange(0, 5)
    speed = [lander_start_speed_h, lander_start_speed_v]
    landerrect = LANDER.get_rect()
    landerrect = landerrect.move([lander_start_h, lander_start_v])

    rocklist = rock_init(rocklist, WIDTH, HEIGHT)

    on_rescue = 0
    rescue = rescue_init(WIDTH)

    on_base = 0

    base_out_count = 0

    if new_level:
        baserect, base_out, base_out_count, habitatrect = level_init(
            baserect, base_out, base_out_count, habitatrect, WIDTH, floor
        )
    else:
        if not base_out:
            base_out_count = 5
        RESCUE.out = 1

    return (
        direction,
        fuel,
        dead,
        lost,
        destroyed,
        deflector,
        landerrect,
        speed,
        rocklist,
        on_base,
        base_out,
        base_out_count,
        baserect,
        habitatrect,
        rescue,
        on_rescue,
    )


(
    DIR,
    FUEL,
    DEAD,
    LOST,
    DESTROYED,
    DEFLECTOR,
    LANDERRECT,
    SPEED,
    ROCKLIST,
    ON_BASE,
    BASE_OUT,
    BASE_OUT_COUNT,
    BASERECT,
    HABITATRECT,
    RESCUE,
    ON_RESCUE,
) = game_init(LANDERRECT, ROCKLIST, BASERECT, HABITATRECT, BASE_OUT, 1, FLOOR)


while 1:
    THRUST = 0
    DEFLECT = 0
    DEFLECTOR_RANGE = DEFLECTOR / 5
    DEFLECT_RATE = DEFLECTOR_RANGE / 10

    ON_FLOOR = 0
    LANDER_BURNING = 0

    for THISROCK in ROCKLIST:
        THISROCK.burning = 0

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_p:
                pause_text = BIG_FONT.render("Paused. . .", 1, COLOR)
                SCREEN.blit(pause_text, ((WIDTH / 2) - 100, (HEIGHT / 2) - 100))
                pygame.display.flip()
                PAUSELOOP = 0
                while PAUSELOOP == 0:
                    for pevent in pygame.event.get():
                        if pevent.type == pygame.KEYDOWN and pevent.key == pygame.K_p:
                            PAUSELOOP = 1
            if event.key == pygame.K_s:
                if ARGS.nosound == 1:
                    sound_text = BIG_FONT.render("Sound off!", 1, COLOR)
                    ARGS.nosound = 0
                else:
                    sound_text = BIG_FONT.render("Sound on!", 1, COLOR)
                    ARGS.nosound = 1
                SCREEN.blit(sound_text, ((WIDTH / 2) - 100, (HEIGHT / 2) - 100))
                pygame.display.flip()

    KEY = pygame.key.get_pressed()

    CENTER = LANDERRECT.center
    LANDER_BURN_CENTER = LANDER_BURNRECT.center

    if KEY[pygame.K_q]:
        pygame.mouse.set_visible(1)
        sys.exit()

    if KEY[pygame.K_r]:
        ROCKLIST = rockburst(ROCKLIST, WIDTH, HEIGHT)
        SCORE += 50

    if KEY[pygame.K_LEFT]:
        if FUEL >= 0.5 and LANDERRECT.bottom < FLOOR and not ON_BASE and not ON_RESCUE:
            DIR += 6
            FUEL -= 0.5

    if KEY[pygame.K_RIGHT]:
        if FUEL >= 0.5 and LANDERRECT.bottom < FLOOR and not ON_BASE and not ON_RESCUE:
            DIR -= 6
            FUEL -= 0.5

    if KEY[pygame.K_SPACE] and DEFLECTOR > 0 and not ON_BASE and not ON_RESCUE:
        if ARGS.nosound:
            SHIELD_CHANNEL.play(SHIELD_SOUND, -1)
        DEFLECTOR -= 1
        DEFLECT = 1
    else:
        if ARGS.nosound:
            SHIELD_CHANNEL.stop()

    ON_BASE = 0
    ON_RESCUE = 0

    LANDERNEW = pygame.transform.rotate(LANDER, DIR)
    LANDERRECT = LANDERNEW.get_rect(center=LANDERRECT.center)
    LANDER_THRUSTNEW = pygame.transform.rotate(LANDER_THRUST, DIR)

    # lander burn
    TOTAL_SPEED = math.sqrt(math.pow(SPEED[0], 2) + math.pow(SPEED[1], 2))
    ATMO_QUOT = 1200 - FLOOR - LANDERRECT.centery
    LANDER_BURNNEW = LANDER_BURN
    if (
        TOTAL_SPEED > ATMO_QUOT and TOTAL_SPEED > 8
    ):  # vary by altitute, i.e. atmospheric viscosity
        if ARGS.nosound:
            BURN_CHANNEL.play(BURN_SOUND, -1)
        LANDER_BURNING = 1
        if DEFLECTOR >= 0:
            DEFLECTOR -= int(TOTAL_SPEED)
        else:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            LANDERNEW = pygame.transform.rotate(BOOM, DIR)
            LANDERRECT = LANDERNEW.get_rect(center=CENTER)
            DEAD_TEXT = BIG_FONT.render("FOOF!!!", 1, COLOR)
            DEAD = 1

        LANDER_TRAVEL_DIR = math.degrees(math.atan2(SPEED[0], SPEED[1]))
        LANDER_BURNNEW = pygame.transform.rotate(
            LANDER_BURN, LANDER_TRAVEL_DIR
        )  # get DIR from movement
        LANDER_BURNRECT.centerx = LANDERRECT.centerx - (
            LANDERRECT.width / 2
        )  # center burning on lander
        LANDER_BURNRECT.centery = LANDERRECT.centery - (LANDERRECT.height / 2)

    # rock burn
    ROCK_COUNT = 0
    for THISROCK in ROCKLIST:
        TOTAL_SPEED = math.sqrt(
            math.pow(THISROCK.speed_h, 2) + math.pow(THISROCK.speed_v, 2)
        )
        ATMO_QUOT = 1200 - FLOOR - THISROCK.rect.centery
        if TOTAL_SPEED > ATMO_QUOT and TOTAL_SPEED > 8:
            THISROCK.burning = 1
            THISROCK.mass -= TOTAL_SPEED / (THISROCK.mass * 100)
            if THISROCK.mass <= 3:
                THISROCK = rock_load_images(THISROCK, 3)
            if THISROCK.mass <= 2:
                THISROCK = rock_load_images(THISROCK, 2)
            if THISROCK.mass <= 1:
                THISROCK = rock_load_images(THISROCK, 1)
            if THISROCK.mass <= 0:
                del ROCKLIST[ROCK_COUNT]
                break

        rock_burn_center = THISROCK.burnrect.center
        ROCK_TRAVEL_DIR = math.degrees(math.atan2(THISROCK.speed_h, THISROCK.speed_v))
        THISROCK.burnnew = pygame.transform.rotate(
            THISROCK.burn_image, ROCK_TRAVEL_DIR
        )  # get DIR from movement
        # thisrock.burnrect = thisrock.burnnew.get_rect(center=thisrock.burnrect.center)
        # thisrock.burnrect.centerx = thisrock.rect.centerx-(thisrock.rect.width/2) \
        # center burning on rock
        # thisrock.burnrect.centery = thisrock.rect.centery-(thisrock.rect.height/2)
        ROCK_COUNT += 1

    if not BASE_OUT:
        BASENEW = pygame.transform.rotate(BASE, 0)
    HABITATNEW = pygame.transform.rotate(HABITAT, 0)
    if RESCUE.orient_mult == 1:
        RESCUE.new = pygame.transform.rotate(RESCUE.image_right, 0)
    else:
        RESCUE.new = pygame.transform.rotate(RESCUE.image_left, 0)

    if DIR >= 360:
        DIR -= 360
    if DIR < 0:
        DIR += 360

    for THISROCK in ROCKLIST:
        if THISROCK.DIR >= 360:
            THISROCK.DIR -= 360
        if THISROCK.DIR < 0:
            THISROCK.DIR += 360

    if KEY[pygame.K_UP]:
        if FUEL >= 1:
            if ARGS.nosound:
                THRUST_CHANNEL.play(THRUST_SOUND, -1)
            THRUST = 1
            FUEL -= 1
            DIRSCRATCH = abs(DIR)
            REM = 0
            REM_RESULT = 0
            DIR_RESULT = 0
            if DIRSCRATCH == 0:
                SPEED[1] -= 1

            if 0 < DIRSCRATCH < 90:
                REM = 90 - DIRSCRATCH
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR) / 90
                SPEED[0] -= DIR_RESULT
                SPEED[1] -= REM_RESULT

            if DIRSCRATCH == 90:
                SPEED[0] -= 1

            if 90 < DIRSCRATCH < 180:
                DIR_CALC = DIRSCRATCH - 90
                REM = 90 - DIR_CALC
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR_CALC) / 90
                SPEED[0] -= REM_RESULT
                SPEED[1] += DIR_RESULT

            if DIRSCRATCH == 180:
                SPEED[1] += 1

            if 180 < DIRSCRATCH < 270:
                DIR_CALC = DIRSCRATCH - 180
                REM = 90 - DIR_CALC
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR_CALC) / 90
                SPEED[0] += DIR_RESULT
                SPEED[1] += REM_RESULT

            if DIRSCRATCH == 270:
                SPEED[0] += 1

            if DIRSCRATCH > 270:
                DIR_CALC = DIRSCRATCH - 270
                REM = 90 - DIR_CALC
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR_CALC) / 90
                SPEED[0] += REM_RESULT
                SPEED[1] -= DIR_RESULT
        else:
            if ARGS.nosound:
                THRUST_CHANNEL.stop()

    if KEY[pygame.K_DOWN]:
        if FUEL >= 1:
            if ARGS.nosound:
                THRUST_CHANNEL.play(THRUST_SOUND, -1)
            FUEL -= 0.25
            DIRSCRATCH = abs(DIR)
            REM = 0
            REM_RESULT = 0
            DIR_RESULT = 0
            if DIRSCRATCH == 0:
                SPEED[1] += 0.25

            if 0 < DIRSCRATCH < 90:
                REM = 90 - DIRSCRATCH
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR) / 90
                SPEED[0] += DIR_RESULT / 4
                SPEED[1] += REM_RESULT / 4

            if DIRSCRATCH == 90:
                SPEED[0] += 0.25

            if 90 < DIRSCRATCH < 180:
                DIR_CALC = DIRSCRATCH - 90
                REM = 90 - DIR_CALC
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR_CALC) / 90
                SPEED[0] += REM_RESULT / 4
                SPEED[1] -= DIR_RESULT / 4

            if DIRSCRATCH == 180:
                SPEED[1] -= 0.25

            if 180 < DIRSCRATCH < 270:
                DIR_CALC = DIRSCRATCH - 180
                REM = 90 - DIR_CALC
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR_CALC) / 90
                SPEED[0] -= DIR_RESULT / 4
                SPEED[1] -= REM_RESULT / 4

            if DIRSCRATCH == 270:
                SPEED[0] -= 0.25

            if DIRSCRATCH > 270:
                DIR_CALC = DIRSCRATCH - 270
                REM = 90 - DIR_CALC
                REM_RESULT = float(REM) / 90
                DIR_RESULT = float(DIR_CALC) / 90
                SPEED[0] -= REM_RESULT / 4
                SPEED[1] += DIR_RESULT / 4
        else:
            if ARGS.nosound:
                THRUST_CHANNEL.stop()

    if LANDERRECT.bottom > FLOOR:
        IMPACTSPEED_V = SPEED[1]
        IMPACTSPEED_H = SPEED[0]
        SPEED[1] = 0
        SPEED[0] = 0
        LANDERRECT.bottom = FLOOR
        if (
            (DIR > 350 or DIR < 10)
            and abs(IMPACTSPEED_H) < 2
            and abs(IMPACTSPEED_V) < 5
        ):
            DIR = 0
        else:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            LANDERNEW = pygame.transform.rotate(BOOM, DIR)
            LANDERRECT = LANDERNEW.get_rect(center=CENTER)
            DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
            DEAD = 1

    if LANDERRECT.bottom == FLOOR and SPEED[0] == 0 and SPEED[1] == 0:
        ON_FLOOR = 1
        if abs(LANDERRECT.centerx - HABITATRECT.centerx) <= 75:
            LOAD_DELAY += 1
            if (
                PEOPLE_IN_HABITAT > 0
                and LOAD_DELAY >= LOAD_DELAY_MAX
                and PEOPLE_IN_LANDER < PEOPLE_IN_LANDER_LIMIT
            ):
                if ARGS.nosound:
                    CHANNEL = PICKUP_SOUND.play()
                PEOPLE_IN_HABITAT -= 1
                PEOPLE_IN_LANDER += 1
                LOAD_DELAY = 0

    if (
        LANDERRECT.bottom > BASERECT.top
        and abs(LANDERRECT.centerx - BASERECT.centerx) <= 16
        and not ON_BASE
        and not BASE_OUT
    ):
        IMPACTSPEED_V = SPEED[1]
        IMPACTSPEED_H = SPEED[0]
        SPEED[1] = 0
        SPEED[0] = 0
        LANDERRECT.bottom = BASERECT.top
        if (
            (DIR > 350 or DIR < 10)
            and abs(IMPACTSPEED_H) < 2
            and abs(IMPACTSPEED_V) < 5
        ):
            DIR = 0
        else:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            LANDERNEW = pygame.transform.rotate(BOOM, DIR)
            LANDERRECT = LANDERNEW.get_rect(center=CENTER)
            DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
            DEAD = 1

    if (
        LANDERRECT.bottom == BASERECT.top
        and SPEED[0] == 0
        and SPEED[1] == 0
        and not BASE_OUT
    ):
        ON_BASE = 1
        if FUEL < 200:
            FUEL += 1
        if DEFLECTOR < 500:
            DEFLECTOR += 0.5

    if (
        LANDERRECT.bottom > RESCUE.rect.top
        and LANDERRECT.top < RESCUE.rect.bottom
        and abs(LANDERRECT.centerx - RESCUE.rect.centerx) <= 20
        and RESCUE.out == 0
    ):
        IMPACTSPEED_V = SPEED[1]
        IMPACTSPEED_H = SPEED[0]
        SPEED[1] = RESCUE.speed_v
        SPEED[0] = RESCUE.speed_h
        LANDERRECT.bottom = RESCUE.rect.top
        if (
            (DIR > 350 or DIR < 10)
            and abs(IMPACTSPEED_H) < 3
            and abs(IMPACTSPEED_V) < 6
        ):
            DIR = 0
        else:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            LANDERNEW = pygame.transform.rotate(BOOM, DIR)
            LANDERRECT = LANDERNEW.get_rect(center=CENTER)
            DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
            DEAD = 1

    if (
        LANDERRECT.bottom == RESCUE.rect.top
        and SPEED[0] == RESCUE.speed_h
        and SPEED[1] == RESCUE.speed_v
        and RESCUE.out == 0
    ):
        ON_RESCUE = 1
        LOAD_DELAY += 1
        if FUEL < 200:
            FUEL += 0.5
        if DEFLECTOR < 500:
            DEFLECTOR += 0.25
        if PEOPLE_IN_LANDER > 0 and LOAD_DELAY >= LOAD_DELAY_MAX:
            if ARGS.nosound:
                CHANNEL = DROPOFF_SOUND.play()
            PEOPLE_IN_LANDER -= 1
            PEOPLE_IN_RESCUE += 1
            SCORE += 50
            LOAD_DELAY = 0

    ROCK_COUNT = 0
    for THISROCK in ROCKLIST:
        if THISROCK.rect.centery > FLOOR:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            THISROCK.speed_h, THISROCK.speed_v = (0, 0)
            THISROCK.rect.centery = FLOOR
            THISROCK.spin = 0
            THISROCK.CENTERHOLD = THISROCK.rect.center
            THISROCK.new = pygame.transform.rotate(THISROCK.image_boom, THISROCK.DIR)
            THISROCK.rect = THISROCK.new.get_rect(center=THISROCK.CENTERHOLD)
            kabam(THISROCK.new, THISROCK.rect)
            SCORE += abs(THISROCK.rect.centerx - HABITATRECT.centerx)
            del ROCKLIST[ROCK_COUNT]
        ROCK_COUNT += 1

    if LANDERRECT.bottom < FLOOR - 5000:
        if RESCUE.out == 0:
            LOST_TEXT = BIG_FONT.render("LOST IN SPACE!!!", 1, COLOR)
            LOST = 1
        else:
            SCORE += PEOPLE_IN_LANDER * 100
            ENDLEVEL = 1

    for THISROCK in ROCKLIST:
        distance = math.sqrt(
            math.pow(abs(LANDERRECT.centerx - THISROCK.rect.centerx), 2)
            + math.pow(abs(LANDERRECT.centery - THISROCK.rect.centery), 2)
        )
        if distance < DEFLECTOR_RANGE and DEFLECT == 1:
            DEFLECTOR -= DEFLECT_RATE * 10
            DEFLECTOR = max(DEFLECTOR, 0)
            kick = DEFLECT_RATE - (distance / 50)
            rock_kick = kick / THISROCK.mass
            lander_kick = kick * THISROCK.mass / 3
            if LANDERRECT.centerx > THISROCK.rect.centerx:
                if THISROCK.speed_h > 0:
                    THISROCK.speed_h -= rock_kick
                SPEED[0] += abs(lander_kick - 1)
            else:
                if THISROCK.speed_h > 0:
                    THISROCK.speed_h += rock_kick
                SPEED[0] -= abs(lander_kick - 1)
            if LANDERRECT.centery > THISROCK.rect.centery:
                if THISROCK.speed_v > 0:
                    THISROCK.speed_v -= rock_kick
                SPEED[1] += abs(lander_kick - 1)
            else:
                if THISROCK.speed_v > 0:
                    THISROCK.speed_v += rock_kick
                SPEED[1] -= abs(lander_kick - 1)

    if FLOOR - LANDERRECT.centery < DEFLECTOR_RANGE and DEFLECT == 1:
        DEFLECTOR -= DEFLECT_RATE * 10
        DEFLECTOR = max(DEFLECTOR, 0)
        SPEED[1] -= abs(DEFLECT_RATE - 1)

    for THISROCK in ROCKLIST:
        if LANDERRECT.colliderect(THISROCK.rect):
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            LANDERNEW = pygame.transform.rotate(BOOM, DIR)
            LANDERRECT = LANDERNEW.get_rect(center=CENTER)
            DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
            DEAD = 1

    ROCK_COUNT = 0
    for THISROCK in ROCKLIST:
        if THISROCK.rect.colliderect(HABITATRECT) and not HABITAT_OUT:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            THISROCK.speed_h, THISROCK.speed_v = (0, 0)
            THISROCK.spin = 0
            THISROCK.CENTERHOLD = THISROCK.rect.center
            THISROCK.new = pygame.transform.rotate(THISROCK.image_boom, THISROCK.DIR)
            THISROCK.rect = THISROCK.new.get_rect(center=THISROCK.CENTERHOLD)
            kabam(THISROCK.new, THISROCK.rect)
            del ROCKLIST[ROCK_COUNT]
            HABITATNEW = pygame.transform.rotate(HABITAT_BOOM, 0)
            HABITATRECT = HABITATNEW.get_rect(center=HABITATRECT.center)
            HABITATRECT.bottom = FLOOR
            if PEOPLE_IN_RESCUE + PEOPLE_IN_LANDER == 0:
                DESTROYED = 1
            else:
                PEOPLE_IN_HABITAT = 0
                HABITAT_OUT = 1
        ROCK_COUNT += 1

    if LANDERRECT.colliderect(HABITATRECT) and not HABITAT_OUT:
        if ARGS.nosound:
            CHANNEL = CRASH_SOUND.play()
        LANDERNEW = pygame.transform.rotate(BOOM, DIR)
        LANDERRECT = LANDERNEW.get_rect(center=CENTER)
        DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
        DEAD = 1
        HABITATNEW = pygame.transform.rotate(HABITAT_BOOM, 0)
        HABITATRECT = HABITATNEW.get_rect(center=HABITATRECT.center)
        HABITATRECT.bottom = FLOOR
        if PEOPLE_IN_RESCUE + PEOPLE_IN_LANDER == 0:
            DESTROYED = 1
        PEOPLE_IN_HABITAT = 0
        HABITAT_OUT = 1

    if LANDERRECT.colliderect(RESCUE.rect) and not RESCUE.out:
        if ARGS.nosound:
            CHANNEL = CRASH_SOUND.play()
        LANDERNEW = pygame.transform.rotate(BOOM, DIR)
        LANDERRECT = LANDERNEW.get_rect(center=CENTER)
        DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
        DEAD = 1
        if RESCUE.orient_mult == 1:
            RESCUE.new = pygame.transform.rotate(RESCUE.image_right_boom, 0)
        else:
            RESCUE.new = pygame.transform.rotate(RESCUE.image_left_boom, 0)
        RESCUE.rect = RESCUE.new.get_rect(center=RESCUE.rect.center)
        RESCUE.out = 1

    ROCK_COUNT = 0
    for THISROCK in ROCKLIST:
        if THISROCK.rect.colliderect(BASERECT) and not BASE_OUT:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            THISROCK.speed_h, THISROCK.speed_v = (0, 0)
            THISROCK.spin = 0
            THISROCK.CENTERHOLD = THISROCK.rect.center
            THISROCK.new = pygame.transform.rotate(THISROCK.image_boom, THISROCK.DIR)
            THISROCK.rect = THISROCK.new.get_rect(center=THISROCK.CENTERHOLD)
            kabam(THISROCK.new, THISROCK.rect)
            time.sleep(0.25)
            del ROCKLIST[ROCK_COUNT]
            BASENEW = pygame.transform.rotate(BASE_BOOM, 0)
            BASERECT = BASENEW.get_rect(center=BASERECT.center)
            BASE_OUT = 1
            BASERECT.bottom = FLOOR
            if ON_BASE:
                if ARGS.nosound:
                    CHANNEL = CRASH_SOUND.play()
                LANDERNEW = pygame.transform.rotate(BOOM, DIR)
                LANDERRECT = LANDERNEW.get_rect(center=CENTER)
                DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
                DEAD = 1
        ROCK_COUNT += 1

    ROCK_COUNT = 0
    for THISROCK in ROCKLIST:
        if THISROCK.rect.colliderect(RESCUE.rect) and not RESCUE.out:
            if ARGS.nosound:
                CHANNEL = CRASH_SOUND.play()
            THISROCK.speed_h, THISROCK.speed_v = (0, 0)
            THISROCK.spin = 0
            THISROCK.CENTERHOLD = THISROCK.rect.center
            THISROCK.new = pygame.transform.rotate(THISROCK.image_boom, THISROCK.DIR)
            THISROCK.rect = THISROCK.new.get_rect(center=THISROCK.CENTERHOLD)
            kabam(THISROCK.new, THISROCK.rect)
            time.sleep(0.25)
            del ROCKLIST[ROCK_COUNT]
            if RESCUE.orient_mult == 1:
                RESCUE.new = pygame.transform.rotate(RESCUE.image_right_boom, 0)
            else:
                RESCUE.new = pygame.transform.rotate(RESCUE.image_left_boom, 0)
            RESCUE.rect = RESCUE.new.get_rect(center=RESCUE.rect.center)
            RESCUE.out = 1
            if ON_RESCUE:
                if ARGS.nosound:
                    CHANNEL = CRASH_SOUND.play()
                LANDERNEW = pygame.transform.rotate(BOOM, DIR)
                LANDERRECT = LANDERNEW.get_rect(center=CENTER)
                DEAD_TEXT = BIG_FONT.render("CRASH!!!", 1, COLOR)
                DEAD = 1
        ROCK_COUNT += 1

    ROCK_A_COUNTER = 0
    for THISROCK in ROCKLIST:
        ROCK_B_COUNTER = 0
        for THATROCK in ROCKLIST:
            if (
                THISROCK.rect.colliderect(THATROCK.rect)
                and ROCK_A_COUNTER != ROCK_B_COUNTER
            ):
                if ARGS.nosound:
                    CHANNEL = CRASH_SOUND.play()
                if THISROCK.mass >= THATROCK.mass:
                    THISROCK.mass += THATROCK.mass
                    del ROCKLIST[ROCK_B_COUNTER]
                    if THISROCK.mass <= 3:
                        THISROCK = rock_load_images(THISROCK, 3)
                    if THISROCK.mass <= 2:
                        THISROCK = rock_load_images(THISROCK, 2)
                    if THISROCK.mass <= 1:
                        THISROCK = rock_load_images(THISROCK, 1)
                    THISROCK.speed_h = THATROCK.speed_h / 5
                    THISROCK.speed_v = THATROCK.speed_v / 5
                else:
                    THATROCK.mass += THISROCK.mass
                    del ROCKLIST[ROCK_A_COUNTER]
                    if THATROCK.mass <= 3:
                        THATROCK = rock_load_images(THATROCK, 3)
                    if THATROCK.mass <= 2:
                        THATROCK = rock_load_images(THATROCK, 2)
                    if THATROCK.mass <= 1:
                        THATROCK = rock_load_images(THATROCK, 1)
                    THATROCK.speed_h = THISROCK.speed_h / 5
                    THATROCK.speed_v = THISROCK.speed_v / 5
            ROCK_B_COUNTER += 1
        ROCK_A_COUNTER += 1

    for THISROCK in ROCKLIST:
        THISROCK.DIR += THISROCK.spin
        THISROCK.CENTERHOLD = THISROCK.rect.center
        THISROCK.new = pygame.transform.rotate(THISROCK.image, THISROCK.DIR)
        THISROCK.rect = THISROCK.new.get_rect(center=THISROCK.CENTERHOLD)

    for THISROCK in ROCKLIST:
        THISROCK.rect.centerx = wrap(THISROCK.rect.centerx, WIDTH)

    for THISROCK in ROCKLIST:
        if THISROCK.rect.centery < FLOOR:
            THISROCK.speed_v = gravitize(THISROCK.speed_v, FLOOR, THISROCK.rect.centery)

    # RESCUE evasive action
    if not RESCUE.out and RESCUE.rect.centery < 500 and RESCUE.entering:
        RESCUE.speed_v = 2
        if ON_RESCUE:
            SPEED[1] = 2
    else:
        ROCKLISTLEN = len(ROCKLIST)
        if ROCKLISTLEN > 0:
            for THISROCK in ROCKLIST:
                if (
                    THISROCK.rect.centery < RESCUE.rect.centery
                    and abs(THISROCK.rect.centerx - RESCUE.rect.centerx) < 400
                    and (FLOOR - RESCUE.rect.centery) > 200
                    and abs(THISROCK.rect.centery - RESCUE.rect.centery) < 400
                ):
                    RESCUE.speed_v = 1
                    if THISROCK.rect.centerx < RESCUE.rect.centerx:
                        RESCUE.speed_h = 2 * RESCUE.orient_mult
                        if ON_RESCUE:
                            SPEED[0] = 2 * RESCUE.orient_mult
                    else:
                        RESCUE.speed_v = 1 * RESCUE.orient_mult
                        if ON_RESCUE:
                            SPEED[0] = 1 * RESCUE.orient_mult
        else:
            RESCUE.speed_h = RESCUE.orig_speed_h
            if ON_RESCUE:
                SPEED[0] = RESCUE.orig_speed_h
        if FLOOR - RESCUE.rect.centery < 400:
            RESCUE.speed_v = -1
            if ON_RESCUE:
                SPEED[1] = -1
        else:
            RESCUE.speed_v = 0
            if ON_RESCUE:
                SPEED[1] = 0

    # wrap
    LANDERRECT.centerx = wrap(LANDERRECT.centerx, WIDTH)
    RESCUE.rect.centerx = wrap(RESCUE.rect.centerx, WIDTH)

    if LANDERRECT.bottom < FLOOR and not ON_BASE and not ON_RESCUE:
        SPEED[1] = gravitize(SPEED[1], FLOOR, LANDERRECT.centery)

    LANDERRECT = LANDERRECT.move(SPEED)
    for THISROCK in ROCKLIST:
        THISROCK.rect = THISROCK.rect.move([THISROCK.speed_h, THISROCK.speed_v])
    if not RESCUE.out:
        RESCUE.rect = RESCUE.rect.move([RESCUE.speed_h, RESCUE.speed_v])

    if RESCUE.entering and RESCUE.rect.centery >= 500:
        RESCUE.entering = 0

    if RESCUE.out == 1:
        PEOPLE_IN_RESCUE = 0

    FUEL_TEXT = FONT.render("Fuel: " + str(FUEL), 1, COLOR)
    VSPEED_TEXT = FONT.render("V: " + str(int(SPEED[1])), 1, COLOR)
    HSPEED_TEXT = FONT.render("H: " + str(int(SPEED[0])), 1, COLOR)
    ALTITUDE_TEXT = FONT.render("Alt: " + str(FLOOR - LANDERRECT.bottom), 1, COLOR)
    ATTITUDE_TEXT = FONT.render("Att: " + str(abs(DIR)), 1, COLOR)
    DEFLECTOR_TEXT = FONT.render("Deflector: " + str(int(DEFLECTOR)), 1, COLOR)
    SCORE_TEXT = FONT.render("Score: " + str(SCORE), 1, COLOR)
    LANDERS_TEXT = FONT.render("Landers: " + str(LANDERS), 1, COLOR)
    PEOPLE_TEXT = FONT.render(
        "People: Habitat: "
        + str(PEOPLE_IN_HABITAT)
        + " Lander: "
        + str(PEOPLE_IN_LANDER)
        + " Rescued: "
        + str(PEOPLE_IN_RESCUE),
        1,
        COLOR,
    )
    LEVEL_TEXT = FONT.render("Level: " + str(LEVEL), 1, COLOR)
    SCREEN.fill(BLACK)
    FLOOR_LINE = pygame.draw.line(SCREEN, COLOR, (0, FLOOR), (WIDTH, FLOOR), 1)
    for star in STARS:
        star_circle = pygame.draw.circle(SCREEN, COLOR, star, 2, 2)
    for THISROCK in ROCKLIST:
        SCREEN.blit(THISROCK.new, THISROCK.rect)
        if THISROCK.burning:
            SCREEN.blit(THISROCK.burnnew, THISROCK.rect)
    if BASE_OUT_COUNT > 0 and BASE_OUT:
        BASE_OUT_COUNT -= 1
    if BASE_OUT_COUNT > 0:
        SCREEN.blit(BASENEW, BASERECT)
    if RESCUE.out == 0:
        SCREEN.blit(RESCUE.new, RESCUE.rect)
    if HABITAT_OUT_COUNT > 0 and HABITAT_OUT:
        HABITAT_OUT_COUNT -= 1
    if HABITAT_OUT_COUNT > 0:
        SCREEN.blit(HABITATNEW, HABITATRECT)
    if THRUST:
        SCREEN.blit(LANDER_THRUSTNEW, LANDERRECT)
    else:
        SCREEN.blit(LANDERNEW, LANDERRECT)
    if LANDER_BURNING:
        SCREEN.blit(LANDER_BURNNEW, LANDER_BURNRECT)
    if DEFLECT and DEFLECTOR_RANGE >= 1:
        DEFLECTOR_LINE = pygame.draw.circle(
            SCREEN, COLOR, LANDERRECT.center, int(DEFLECTOR_RANGE), 1
        )
    SCREEN.blit(FUEL_TEXT, (10, INFOBAR_H))
    SCREEN.blit(VSPEED_TEXT, (90, INFOBAR_H))
    SCREEN.blit(HSPEED_TEXT, (140, INFOBAR_H))
    SCREEN.blit(ALTITUDE_TEXT, (195, INFOBAR_H))
    SCREEN.blit(ATTITUDE_TEXT, (270, INFOBAR_H))
    SCREEN.blit(DEFLECTOR_TEXT, (325, INFOBAR_H))
    SCREEN.blit(SCORE_TEXT, (450, INFOBAR_H))
    SCREEN.blit(LANDERS_TEXT, (550, INFOBAR_H))
    SCREEN.blit(PEOPLE_TEXT, (650, INFOBAR_H))
    SCREEN.blit(LEVEL_TEXT, (925, INFOBAR_H))
    if DEAD:
        SCREEN.blit(DEAD_TEXT, ((WIDTH / 2) - 125, (HEIGHT / 2) - 150))
    if LOST:
        SCREEN.blit(LOST_TEXT, ((WIDTH / 2) - 200, (HEIGHT / 2) - 150))
    pygame.display.flip()

    CLOCK.tick(20)

    # regen rocks
    if ROCK_DELAY >= ROCK_DELAY_MAX:
        if len(ROCKLIST) < LEVEL:
            if random.randrange(0, 100) > 42:
                ROCKLIST = rock_init(ROCKLIST, WIDTH, HEIGHT)
            ROCK_DELAY = 0
    else:
        ROCK_DELAY += 1

    if random.randrange(0, 100) > 99:
        ROCKLIST = rockburst(ROCKLIST, WIDTH, HEIGHT)

    # check if we LOST a lander
    if DEAD:
        PEOPLE_IN_LANDER = 0
        if LANDERS > 0:
            LANDERS -= 1
        else:
            ENDGAME = 1

    # check if we ended the LEVEL
    if (
        RESCUE.out == 0
        and PEOPLE_IN_LANDER == 0
        and (PEOPLE_IN_HABITAT == 0 or HABITAT_OUT == 1)
        and PEOPLE_IN_RESCUE > 0
    ):
        ENDLEVEL = 1

    if PEOPLE_IN_HABITAT + PEOPLE_IN_LANDER + PEOPLE_IN_RESCUE == 0:
        ENDGAME = 1

    # check if we ended the game
    if LANDERS <= 0 or LOST or DESTROYED:
        ENDGAME = 1

    if ENDGAME == 1:
        # end the game
        if ARGS.nosound:
            pygame.mixer.stop()
        SCOREFILENAME = os.path.join(
            os.path.expanduser("~" + getpass.getuser()), ".iapetal_score"
        )
        if not os.path.isfile(SCOREFILENAME):
            open(  # pylint: disable=consider-using-with
                SCOREFILENAME, "a", encoding="utf-8"
            ).close()
        with open(SCOREFILENAME, "r", encoding="utf-8") as SCOREFILE:
            OLDSCORE = SCOREFILE.readline()
        if not OLDSCORE:
            OLDSCORE = 0
        if SCORE > int(OLDSCORE):
            with open(SCOREFILENAME, "w", encoding="utf-8") as SCOREFILE:
                SCOREFILE.write(str(SCORE) + "\n")
        DESTROYED_TEXT = BIG_FONT.render("GAME OVER", 1, COLOR)
        SCREEN.blit(DESTROYED_TEXT, ((WIDTH / 2) - 150, (HEIGHT / 2) - 100))
        ENDSCORE_TEXT = BIG_FONT.render(str(SCORE), 1, COLOR)
        SCREEN.blit(ENDSCORE_TEXT, ((WIDTH / 2) - 75, (HEIGHT / 2) - 50))
        pygame.display.flip()
        time.sleep(1)
        # . . .and start a new game after a short break
        NEWGAME_TEXT = BIG_FONT.render("Play again (y/n)?", 1, COLOR)
        SCREEN.blit(NEWGAME_TEXT, ((WIDTH / 2) - 200, (HEIGHT / 2)))
        pygame.display.flip()
        time.sleep(2)
        QUITLOOP = 0
        while QUITLOOP == 0:
            for qevent in pygame.event.get():
                if qevent.type == pygame.KEYDOWN and qevent.key == pygame.K_n:
                    sys.exit()
                if qevent.type == pygame.KEYDOWN and qevent.key == pygame.K_y:
                    QUITLOOP = 1
        SCORE = 0
        LANDERS = 3
        ENDGAME = 0
        PEOPLE_IN_HABITAT = 16
        PEOPLE_IN_LANDER = 0
        PEOPLE_IN_RESCUE = 0
        LEVEL = 1
        ROCKLIST = []
        HABITAT_OUT = 0
        BASE_OUT = 0
        HABITAT_OUT_COUNT = 5
        STARS = stars_init(STARS, WIDTH, FLOOR)
        (
            DIR,
            FUEL,
            DEAD,
            LOST,
            DESTROYED,
            DEFLECTOR,
            LANDERRECT,
            SPEED,
            ROCKLIST,
            ON_BASE,
            BASE_OUT,
            BASE_OUT_COUNT,
            BASERECT,
            HABITATRECT,
            RESCUE,
            ON_RESCUE,
        ) = game_init(LANDERRECT, ROCKLIST, BASERECT, HABITATRECT, BASE_OUT, 1, FLOOR)
    else:
        # if not, move on
        if ENDLEVEL == 1:
            # new LEVEL block
            if ARGS.nosound:
                pygame.mixer.stop()
            SCORE += PEOPLE_IN_RESCUE * 100
            if not BASE_OUT:
                SCORE += 500
            PEOPLE_IN_HABITAT = 16
            PEOPLE_IN_RESCUE = 0
            PEOPLE_IN_LANDER = 0
            HABITAT_OUT = 0
            HABITAT_OUT_COUNT = 5
            RESCUE.out = 0
            BASE_OUT = 0
            LEVEL += 1
            ROCKLIST = []
            time.sleep(1)
            (
                DIR,
                FUEL,
                DEAD,
                LOST,
                DESTROYED,
                DEFLECTOR,
                LANDERRECT,
                SPEED,
                ROCKLIST,
                ON_BASE,
                BASE_OUT,
                BASE_OUT_COUNT,
                BASERECT,
                HABITATRECT,
                RESCUE,
                ON_RESCUE,
            ) = game_init(
                LANDERRECT, ROCKLIST, BASERECT, HABITATRECT, BASE_OUT, ENDLEVEL, FLOOR
            )
            STARS = stars_init(STARS, WIDTH, FLOOR)
            ENDLEVEL = 0
        if DEAD == 1:
            # spawn player, and LEVEL if appropriate
            time.sleep(1)
            (
                DIR,
                FUEL,
                DEAD,
                LOST,
                DESTROYED,
                DEFLECTOR,
                LANDERRECT,
                SPEED,
                ROCKLIST,
                ON_BASE,
                BASE_OUT,
                BASE_OUT_COUNT,
                BASERECT,
                HABITATRECT,
                RESCUE,
                ON_RESCUE,
            ) = game_init(
                LANDERRECT, ROCKLIST, BASERECT, HABITATRECT, BASE_OUT, ENDLEVEL, FLOOR
            )
